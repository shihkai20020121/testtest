# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.1.2](https://gitlab.com/shihkai20020121/testtest/compare/v1.1.1...v1.1.2) (2021-09-27)


### Bug Fixes

* update. ([dee7069](https://gitlab.com/shihkai20020121/testtest/commit/dee70692a8f75edf1430d2e73c2cfb4d73049f7b))

### [1.1.1](https://gitlab.com/shihkai20020121/testtest/compare/v1.1.0...v1.1.1) (2021-09-25)


### Bug Fixes

* missing issues. ([186b44b](https://gitlab.com/shihkai20020121/testtest/commit/186b44b677ff34d1546a699daeb1e663fc9901d8))

## [1.1.0](https://gitlab.com/shihkai20020121/testtest/compare/v1.0.0...v1.1.0) (2021-09-25)


### Features

* append new package about moment. ([968de91](https://gitlab.com/shihkai20020121/testtest/commit/968de9159d016bababa34b3b6ebd8a1461c26e4f))

## 1.0.0 (2021-09-25)


### Features

* append default settings. ([2ae0be5](https://gitlab.com/shihkai20020121/testtest/commit/2ae0be526398477d19216164570e6093ad83af67))
